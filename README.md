# pr4000b

Platform agnostic driver for the MKS PR4000B Power Supply and Readout for Flow and Pressure. Based  on the `embedded-hal` traits.
