use std::{cell::RefCell, convert::Infallible};

use pr4000b::{
    data::{self, Integer},
    error::{DeviceError, Error},
    param, sp, Pr4000b, SerialRead, SerialWrite,
};

#[derive(Default)]
struct FakeIo {
    rx: Box<[u8]>,
    tx: Vec<u8>,
    rx_index: usize,
}

impl FakeIo {
    fn tx(&self) -> &str {
        std::str::from_utf8(&self.tx).unwrap()
    }
}

#[derive(Default)]
struct FakeSerial(RefCell<FakeIo>);

impl SerialRead<u8> for &FakeSerial {
    type Error = Infallible;

    fn read(&mut self) -> nb::Result<u8, Self::Error> {
        let mut io = self.0.borrow_mut();

        if io.rx_index < io.rx.len() {
            let byte = io.rx[io.rx_index];
            io.rx_index += 1;
            Ok(byte)
        } else {
            Err(nb::Error::WouldBlock)
        }
    }
}

impl SerialWrite<u8> for &FakeSerial {
    type Error = Infallible;

    fn write(&mut self, byte: u8) -> nb::Result<(), Self::Error> {
        self.0.borrow_mut().tx.push(byte);
        Ok(())
    }

    fn flush(&mut self) -> nb::Result<(), Self::Error> {
        Ok(())
    }
}

#[test]
fn dialog() {
    let serial = FakeSerial::default();
    let mut device = Pr4000b::new(&serial);

    macro_rules! dialog {
        { op: $op:expr, tx: $tx:expr, rx: $rx:expr, expect: $expect:expr, } => {{
            let mut op = $op;
            assert_eq!(op.poll(), Err(nb::Error::WouldBlock));

            {
                let mut io = serial.0.borrow_mut();
                assert_eq!(io.tx(), $tx);

                io.tx.clear();
                io.rx = String::from($rx).into_bytes().into_boxed_slice();
                io.rx_index = 0;
            }

            let result = op.poll().map_err(|error| match error {
                nb::Error::Other(error) => error,
                nb::Error::WouldBlock => panic!(
                    "Dialog deadlocked: < \"{}\", > \"{}\"", $tx.escape_debug(), $rx.escape_debug()
                ),
            });

            assert_eq!(result, $expect);
        }}
    }

    dialog! {
        op: device.param(param::DisplayText).set(data::Text::try_from("HELLO").unwrap()),
        tx: "DT,HELLO\r",
        rx: "\r",
        expect: Ok(()),
    };

    dialog! {
        op: device.param(param::DisplayText).read(),
        tx: "?DT\r",
        rx: "\"HELLO\"\r",
        expect: Ok(data::Text::try_from("HELLO").unwrap()),
    };

    dialog! {
        op: device.param(param::DisplayText).reset(),
        tx: "!DT\r",
        rx: "\r",
        expect: Ok(()),
    };

    dialog! {
        op: device.param(param::LastKey).read(),
        tx: "?KY\r",
        rx: "00014,00001\r",
        expect: Ok(data::Keys { last: Some(data::Key::Down), pressed: Integer::try_from(1).unwrap(), }),
    };

    dialog! {
        op: device.param(param::Id).read(),
        tx: "?ID\r",
        rx: "PR42012900000\r",
        expect: Ok(data::Id { version: 01, release: 29, serial: Integer::try_from(0).unwrap(), }),
    };

    dialog! {
        op: device.param(param::Remote).set(true),
        tx: "RT,ON\r",
        rx: "\r",
        expect: Ok(()),
    };

    dialog! {
        op: device.param(param::Remote).read(),
        tx: "?RT\r",
        rx: "ON\r",
        expect: Ok(true),
    };

    dialog! {
        op: device.param(param::Actual).channel(param::Channel1).read(),
        tx: "AV1\r",
        rx: "-010.00\r",
        expect: Ok(data::Setpoint { value: -1000, decimal_point: 2 }),
    };

    dialog! {
        op: device.param(param::Setpoint).channel(param::Channel1).read(),
        tx: "?SP1\r",
        rx: " 050.00\r",
        expect: Ok(data::Setpoint { value: 5000, decimal_point: 2 }),
    };

    dialog! {
        op: device.param(param::Setpoint).channel(param::Channel1).set(sp!(50.69)),
        tx: "SP1,50.690\r",
        rx: " 050.69\r",
        expect: Ok(data::Setpoint { value: 5069, decimal_point: 2 }),
    };

    dialog! {
        op: device.param(param::Status).read(),
        tx: "ST\r",
        rx: "00128\r",
        expect: Ok(data::Status::USER),
    };

    dialog! {
        op: device.param(param::Valve).channel(param::Channel1).read(),
        tx: "?VL1\r",
        rx: "ON\r",
        expect: Ok(true),
    };

    dialog! {
        op: device.param(param::Valve).channel(param::Channel2).read(),
        tx: "?VL2\r",
        rx: "#E010\r",
        expect: Err(Error::Device(DeviceError::Syntax)),
    };

    dialog! {
        op: device.param(param::Valve).channel(param::Channel0).set(true),
        tx: "VL0,ON\r",
        rx: "\r",
        expect: Ok(()),
    };
}
