use proc_macro::TokenStream;
use proc_macro2::TokenStream as TokenStream2;
use quote::quote;
use syn::*;

#[proc_macro]
pub fn sp(item: TokenStream) -> TokenStream {
    setpoint(parse_macro_input!(item as LitFloat))
        .unwrap_or_else(Error::into_compile_error)
        .into()
}

fn setpoint(lit: LitFloat) -> Result<TokenStream2> {
    let span = lit.span();
    if !lit.suffix().is_empty() {
        return Err(Error::new(span, "sp literal cannot have a suffix"));
    }

    let lit_digits = {
        let digits = lit.base10_digits();

        let first_non_zero = digits
            .bytes()
            .enumerate()
            .find_map(|(i, c)| (c != b'0').then(|| i))
            .unwrap_or_else(|| digits.len() - 1);

        let last_non_zero = digits
            .bytes()
            .enumerate()
            .rev()
            .find_map(|(i, c)| (c != b'0').then(|| i))
            .unwrap_or(0);

        &digits[first_non_zero..=last_non_zero]
    };

    let point_index = match lit_digits.chars().enumerate().find(|(_, c)| *c == '.') {
        Some((point_index, _)) => point_index,
        None => return Err(Error::new(span, "expected decimal point")),
    };

    if lit_digits.len() > 5 + 1 {
        return Err(Error::new(span, "too many digits for pr4000b"));
    }

    let extra_frac = 6 - lit_digits.len();

    let mut corrected: String = lit_digits.chars().filter(|c| *c != '.').collect();
    (0..extra_frac).for_each(|_| corrected.push('0'));

    let value = LitInt::new(&corrected, span);
    let decimal_point = (lit_digits.len() - 1 - point_index + extra_frac) as u8;

    let expanded = quote! {
        ::pr4000b::data::Setpoint {
            value: #value,
            decimal_point: #decimal_point,
        }
    };

    Ok(expanded)
}
