mod body;
mod error;
mod ops;

pub use embedded_hal::serial::{Read as SerialRead, Write as SerialWrite};

pub use {
    body::{Body, Input, Output},
    error::{DeviceError, DeviceErrorClass},
    ops::Ops,
};

pub trait Serial: SerialRead<u8> + SerialWrite<u8> {}
impl<T: SerialRead<u8> + SerialWrite<u8>> Serial for T {}

pub type ReadError<S> = <S as SerialRead<u8>>::Error;
pub type WriteError<S> = <S as SerialWrite<u8>>::Error;

// See p. 49
pub const MAX_TEXT_LENGTH: usize = 32;

// DT is the worst-case longest command. This length accounts for `DT,`
pub const MAX_CMD_BODY_LENGTH: usize = 3 + MAX_TEXT_LENGTH;

// Replies to DT (enclosed in quotes) are also the longest
pub const MAX_REPLY_LENGTH: usize = 1 + MAX_TEXT_LENGTH + 1;

pub fn is_valid_text(text: &str) -> bool {
    text.len() <= MAX_TEXT_LENGTH && text.chars().all(|c| c == ' ' || c.is_ascii_graphic())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn text() {
        assert!(!is_valid_text("abcdefhijklmnopqrstuvwxyz0123456789"));
        assert!(!is_valid_text("test\r?ID"));

        assert!(is_valid_text(""));
        assert!(is_valid_text("test"));
        assert!(is_valid_text("\"test text\""));
    }
}
