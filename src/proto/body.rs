use crate::{
    data::{self, Integer},
    param::*,
};

use core::{
    fmt::{self, Display, Write},
    str::FromStr,
};

#[derive(Debug, PartialEq, Eq)]
pub struct Body<T>(pub T);

pub trait Input: Sized {
    fn from_str(string: &str) -> Result<Self, ()>;
}

pub trait Output {
    fn fmt(&self, fmt: &mut fmt::Formatter<'_>) -> fmt::Result;
}

impl<T: Input> FromStr for Body<T> {
    type Err = ();

    fn from_str(string: &str) -> Result<Self, Self::Err> {
        T::from_str(string).map(Body)
    }
}

impl<T: Output> Display for Body<T> {
    fn fmt(&self, fmt: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.0.fmt(fmt)
    }
}

impl Input for () {
    fn from_str(string: &str) -> Result<Self, ()> {
        if string.is_empty() {
            Ok(())
        } else {
            Err(())
        }
    }
}

impl Output for () {
    fn fmt(&self, _: &mut fmt::Formatter<'_>) -> fmt::Result {
        Ok(())
    }
}

impl Input for bool {
    fn from_str(string: &str) -> Result<Self, ()> {
        match string {
            "ON" => Ok(true),
            "OFF" => Ok(false),
            _ => Err(()),
        }
    }
}

impl Output for bool {
    fn fmt(&self, fmt: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt.write_str(if *self { "ON" } else { "OFF" })
    }
}

impl Input for Integer {
    fn from_str(string: &str) -> Result<Self, ()> {
        if string.len() == 5 {
            string
                .parse()
                .ok()
                .and_then(|integer: u32| Integer::try_from(integer).ok())
                .ok_or(())
        } else {
            Err(())
        }
    }
}

impl Output for Integer {
    fn fmt(&self, fmt: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(fmt, "{:05}", u32::from(*self))
    }
}

impl Input for data::Text {
    fn from_str(string: &str) -> Result<Self, ()> {
        if string.len() >= 2
            && (string.chars().nth(0), string.chars().last()) == (Some('"'), Some('"'))
        {
            (&string[1..string.len() - 1]).try_into().map_err(|_| ())
        } else {
            Err(())
        }
    }
}

impl Output for data::Text {
    fn fmt(&self, fmt: &mut fmt::Formatter<'_>) -> fmt::Result {
        Display::fmt(self, fmt)
    }
}

impl Output for Channel {
    fn fmt(&self, fmt: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Channel1 => fmt.write_str("1"),
            Channel2 => fmt.write_str("2"),
        }
    }
}

impl Output for Channel0 {
    fn fmt(&self, fmt: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt.write_str("0")
    }
}

impl Input for Option<data::Key> {
    fn from_str(string: &str) -> Result<Self, ()> {
        use data::Key::*;

        match u32::from(Body::<Integer>::from_str(string)?.0) {
            00007 => Ok(Some(Off)),
            00008 => Ok(Some(On)),
            00009 => Ok(Some(Esc)),
            00010 => Ok(Some(Enter)),
            00011 => Ok(Some(Right)),
            00012 => Ok(Some(Left)),
            00013 => Ok(Some(Up)),
            00014 => Ok(Some(Down)),
            00255 => Ok(None),
            _ => Err(()),
        }
    }
}

impl Input for data::Keys {
    fn from_str(string: &str) -> Result<Self, ()> {
        let (last, pressed) = pair(string)?;
        Ok(data::Keys { last, pressed })
    }
}

impl Input for data::Id {
    fn from_str(string: &str) -> Result<Self, ()> {
        if string.len() != 13 || &string[..4] != "PR42" {
            return Err(());
        }

        let (version, release, Body(serial)) = (
            string[4..6].parse().map_err(drop)?,
            string[6..8].parse().map_err(drop)?,
            string[8..].parse()?,
        );

        Ok(data::Id {
            version,
            release,
            serial,
        })
    }
}

impl Input for data::Setpoint {
    fn from_str(string: &str) -> Result<Self, ()> {
        let string = string.trim();

        let mut digits = heapless::String::<{ 5 + 1 }>::new();
        for ch in string.chars().filter(|c| *c != '.') {
            digits.push(ch)?;
        }

        let decimal_point = string
            .char_indices()
            .find(|(_, c)| *c == '.')
            .map(|(n, _)| string.len() - n - 1)
            .unwrap_or(0)
            .try_into()
            .map_err(|_| ())?;

        Ok(data::Setpoint {
            value: digits.parse().map_err(|_| ())?,
            decimal_point,
        })
    }
}

impl Output for data::Setpoint {
    fn fmt(&self, fmt: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut digits = heapless::String::<{ 1 + 5 + 1 }>::new();
        write!(digits, "{:05}", self.value.min(99_999).max(-99_999))?;

        let decimal_point = digits.len() - self.decimal_point.min(5) as usize;

        fmt.write_str(&digits[..decimal_point])?;
        fmt.write_char('.')?;
        fmt.write_str(&digits[decimal_point..])
    }
}

impl Input for data::Status {
    fn from_str(string: &str) -> Result<Self, ()> {
        data::Status::from_bits(Body::<Integer>::from_str(string)?.0.into()).ok_or(())
    }
}

fn pair<L, R>(string: &str) -> Result<(L, R), ()>
where
    Body<L>: FromStr,
    Body<R>: FromStr,
{
    let index = string.find(',').ok_or(())?;

    let (left, right) = string.split_at(index);
    let right = &right[1..];

    let (Body(left), Body(right)) = (
        left.parse().map_err(|_| ())?,
        right.parse().map_err(|_| ())?,
    );

    Ok((left, right))
}

#[cfg(test)]
mod tests {
    extern crate std;

    use super::*;

    #[test]
    fn from_str() {
        assert_eq!(Body::<()>::from_str(""), Ok(Body(())));
        assert_eq!(Body::<()>::from_str("\r"), Err(()));
    }

    #[test]
    fn display() {
        assert_eq!(std::format!("{} - {}", Body(true), Body(false)), "ON - OFF");
        assert_eq!(
            std::format!(
                "{} - {} - {}",
                Body(Channel1),
                Body(Channel2),
                Body(Channel0)
            ),
            "1 - 2 - 0"
        );
    }
}
