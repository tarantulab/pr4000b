use crate::{param::*, Sealed};

pub trait Ops: Sealed {
    /// Whether to send `<prefix><ch>` or `?<prefix><ch>` for reading
    const ESCAPED_READ: bool = true;

    fn prefix(&self) -> &'static str;
}

impl Ops for DisplayText {
    fn prefix(&self) -> &'static str {
        "DT"
    }
}

impl Ops for LastKey {
    fn prefix(&self) -> &'static str {
        "KY"
    }
}

impl Ops for Id {
    fn prefix(&self) -> &'static str {
        "ID"
    }
}

impl Ops for Remote {
    fn prefix(&self) -> &'static str {
        "RT"
    }
}

impl Ops for Current {
    fn prefix(&self) -> &'static str {
        "AC"
    }
}

impl Ops for Actual {
    const ESCAPED_READ: bool = false;

    fn prefix(&self) -> &'static str {
        "AV"
    }
}

impl Ops for Setpoint {
    fn prefix(&self) -> &'static str {
        "SP"
    }
}

impl Ops for External {
    const ESCAPED_READ: bool = false;

    fn prefix(&self) -> &'static str {
        "EX"
    }
}

impl Ops for Status {
    const ESCAPED_READ: bool = false;

    fn prefix(&self) -> &'static str {
        "ST"
    }
}

impl Ops for Valve {
    fn prefix(&self) -> &'static str {
        "VL"
    }
}

impl Ops for Relay {
    fn prefix(&self) -> &'static str {
        "RL"
    }
}

impl Ops for Display {
    fn prefix(&self) -> &'static str {
        match self {
            Display1 => "DP1",
            Display2 => "DP2",
        }
    }
}

impl Ops for (Display, Line) {
    fn prefix(&self) -> &'static str {
        match self {
            (Display1, Line1) => "DP1,1",
            (Display1, Line2) => "DP1,2",
            (Display2, Line1) => "DP2,1",
            (Display2, Line2) => "DP2,2",
        }
    }
}

impl Ops for Display4 {
    fn prefix(&self) -> &'static str {
        "DP4"
    }
}

impl Ops for Range {
    fn prefix(&self) -> &'static str {
        "RG"
    }
}

impl Ops for Gain {
    fn prefix(&self) -> &'static str {
        "GN"
    }
}

impl Ops for Offset {
    fn prefix(&self) -> &'static str {
        "OF"
    }
}

impl Ops for RangeTurndownOffset {
    fn prefix(&self) -> &'static str {
        "RO"
    }
}

impl Ops for IoRange {
    fn prefix(&self) -> &'static str {
        match self {
            Input => "IN",
            Output => "OT",
            ExternalInput => "EI",
            ExternalOutput => "EO",
        }
    }
}

impl Ops for SignalMode {
    fn prefix(&self) -> &'static str {
        "SM"
    }
}

impl Ops for Scale {
    fn prefix(&self) -> &'static str {
        "SC"
    }
}

impl Ops for LimitMode {
    fn prefix(&self) -> &'static str {
        "LM"
    }
}

impl Ops for DeadBand {
    fn prefix(&self) -> &'static str {
        "DB"
    }
}

impl Ops for Limit {
    fn prefix(&self) -> &'static str {
        match self {
            UpperLimit => "UL",
            LowerLimit => "LL",
        }
    }
}

impl Ops for Parity {
    fn prefix(&self) -> &'static str {
        "PY"
    }
}

impl Ops for BaudRate {
    fn prefix(&self) -> &'static str {
        "BD"
    }
}

impl Ops for Address {
    fn prefix(&self) -> &'static str {
        "AD"
    }
}

impl Ops for Resolution {
    fn prefix(&self) -> &'static str {
        "RS"
    }
}
