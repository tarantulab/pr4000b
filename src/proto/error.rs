use core::str::FromStr;

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum DeviceErrorClass {
    E,
    W,
}

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum DeviceError {
    Communication,
    AdcOverflow,
    SetpointRange,
    Syntax,
    CommandFail,
    Offset,
    Unknown(DeviceErrorClass, u16),
}

impl FromStr for DeviceError {
    type Err = ();

    fn from_str(string: &str) -> Result<Self, Self::Err> {
        use {DeviceError::*, DeviceErrorClass::*};

        if string.len() != 5 {
            return Err(());
        }

        let class = match &string[..2] {
            "#E" => E,
            "#W" => W,
            _ => return Err(()),
        };

        let code = string[2..].parse().map_err(|_| ())?;
        let error = match (class, code) {
            (E, 001) => Communication,
            (E, 002) => AdcOverflow,
            (E, 003) => SetpointRange,
            (E, 010) => Syntax,
            (E, 020) => CommandFail,
            (W, 001) => Offset,
            _ => Unknown(class, code),
        };

        Ok(error)
    }
}
