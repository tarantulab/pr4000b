use crate::{data, proto::Ops};

pub trait Param: Ops {
    type Type;
}

pub trait Set: Param {
    type Echo;
}

pub trait Reset: Param {}
pub trait Global: Param {}
pub trait Channelled<C>: Param {}

#[derive(Copy, Clone)]
pub struct Channel0;

#[derive(Copy, Clone)]
pub enum Channel {
    Channel1,
    Channel2,
}

pub use Channel::*;

pub struct DisplayText;

impl Param for DisplayText {
    type Type = data::Text;
}

impl Set for DisplayText {
    type Echo = ();
}

impl Global for DisplayText {}
impl Reset for DisplayText {}

pub struct LastKey;

impl Param for LastKey {
    type Type = data::Keys;
}

impl Global for LastKey {}

//TODO: Dialog

pub struct Id;

impl Param for Id {
    type Type = data::Id;
}

impl Global for Id {}

pub struct Remote;

impl Param for Remote {
    type Type = bool;
}

impl Set for Remote {
    type Echo = ();
}

impl Global for Remote {}
impl Reset for Remote {}

pub struct Current;

impl Param for Current {
    type Type = data::Current;
}

impl Set for Current {
    type Echo = data::Setpoint;
}

impl Channelled<Channel> for Current {}

pub struct Actual;

impl Param for Actual {
    type Type = data::Setpoint;
}

impl Set for Actual {
    type Echo = data::Setpoint;
}

impl Channelled<Channel> for Actual {}

pub struct Setpoint;

impl Param for Setpoint {
    type Type = data::Setpoint;
}

impl Set for Setpoint {
    type Echo = data::Setpoint;
}

impl Channelled<Channel> for Setpoint {}

pub struct External;

impl Param for External {
    type Type = data::External;
}

impl Channelled<Channel> for External {}

pub struct Status;

impl Param for Status {
    type Type = data::Status;
}

impl Global for Status {}

pub struct Valve;

impl Param for Valve {
    type Type = bool;
}

impl Set for Valve {
    type Echo = ();
}

impl Channelled<Channel> for Valve {}
impl Channelled<Channel0> for Valve {}

pub struct Relay;

impl Param for Relay {
    type Type = bool;
}

impl Set for Relay {
    type Echo = ();
}

impl Channelled<Channel> for Relay {}

#[derive(Copy, Clone)]
pub enum Display {
    Display1,
    Display2,
}

pub use Display::*;

impl Param for Display {
    type Type = bool;
}

impl Set for Display {
    type Echo = ();
}

impl Global for Display {}

#[derive(Copy, Clone)]
pub enum Line {
    Line1,
    Line2,
}

pub use Line::*;

impl Param for (Display, Line) {
    type Type = data::Line;
}

impl Set for (Display, Line) {
    type Echo = ();
}

impl Global for (Display, Line) {}

pub struct Display4;

impl Param for Display4 {
    type Type = bool;
}

impl Set for Display4 {
    type Echo = ();
}

impl Global for Display4 {}

pub struct Range;

impl Param for Range {
    type Type = data::Range;
}

impl Set for Range {
    type Echo = ();
}

impl Channelled<Channel> for Range {}

pub struct Gain;

impl Param for Gain {
    type Type = data::Gain;
}

impl Set for Gain {
    type Echo = ();
}

impl Channelled<Channel> for Gain {}

pub struct Offset;

impl Param for Offset {
    type Type = data::Offset;
}

impl Set for Offset {
    type Echo = ();
}

impl Channelled<Channel> for Offset {}
impl Reset for Offset {}

pub struct RangeTurndownOffset;

impl Param for RangeTurndownOffset {
    type Type = data::Offset;
}

impl Set for RangeTurndownOffset {
    type Echo = ();
}

impl Channelled<Channel> for RangeTurndownOffset {}

//TODO: AZ

#[derive(Copy, Clone)]
pub enum IoRange {
    Input,
    Output,
    ExternalInput,
    ExternalOutput,
}

pub use IoRange::*;

impl Param for IoRange {
    type Type = data::VoltageLevel;
}

impl Set for IoRange {
    type Echo = ();
}

impl Channelled<Channel> for IoRange {}

pub struct SignalMode;

impl Param for SignalMode {
    type Type = data::SignalMode;
}

impl Set for SignalMode {
    type Echo = ();
}

impl Global for SignalMode {}

pub struct Scale;

impl Param for Scale {
    type Type = data::Scale;
}

impl Set for Scale {
    type Echo = ();
}

impl Channelled<Channel> for Scale {}

//TODO: LN, LS

pub struct LimitMode;

impl Param for LimitMode {
    type Type = data::LimitMode;
}

impl Set for LimitMode {
    type Echo = ();
}

impl Global for LimitMode {}

pub struct DeadBand;

impl Param for DeadBand {
    type Type = data::DeadBand;
}

impl Set for DeadBand {
    type Echo = ();
}

impl Channelled<Channel> for DeadBand {}

#[derive(Copy, Clone)]
pub enum Limit {
    UpperLimit,
    LowerLimit,
}

pub use Limit::*;

impl Param for Limit {
    type Type = data::LimitBound;
}

impl Set for Limit {
    type Echo = ();
}

impl Channelled<Channel> for Limit {}

//TODO: FR, FT

pub struct Parity;

impl Param for Parity {
    type Type = data::Parity;
}

impl Set for Parity {
    type Echo = ();
}

impl Global for Parity {}

pub struct BaudRate;

impl Param for BaudRate {
    type Type = data::BaudRate;
}

impl Set for BaudRate {
    type Echo = ();
}

impl Global for BaudRate {}

pub struct Address;

impl Param for Address {
    type Type = Address;
}

impl Set for Address {
    type Echo = ();
}

impl Global for Address {}

// Missing docs for IM

pub struct Resolution;

impl Param for Resolution {
    type Type = data::Resolution;
}

impl Set for Resolution {
    type Echo = ();
}

impl Global for Resolution {}

//TODO: RE, DF, #0, #1, $0, $1, ?$
