use bitflags::bitflags;
use core::fmt;

use crate::{
    param,
    proto::{self, MAX_TEXT_LENGTH},
};

/// Unsigned integer of up to 5 decimal digits
#[derive(Copy, Clone, Debug, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct Integer(u32);

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub struct IntegerOverflowError;

impl fmt::Display for Integer {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.0.fmt(formatter)
    }
}

impl TryFrom<u32> for Integer {
    type Error = IntegerOverflowError;

    fn try_from(integer: u32) -> Result<Integer, Self::Error> {
        if integer <= 99_999 {
            Ok(Integer(integer))
        } else {
            Err(IntegerOverflowError)
        }
    }
}

impl From<Integer> for u32 {
    fn from(Integer(integer): Integer) -> u32 {
        integer
    }
}

#[derive(Debug, PartialEq, Eq)]
pub struct Text(heapless::String<MAX_TEXT_LENGTH>);

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub struct InvalidTextError;

impl fmt::Display for Text {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.0.fmt(formatter)
    }
}

impl TryFrom<&str> for Text {
    type Error = InvalidTextError;

    fn try_from(text: &str) -> Result<Text, Self::Error> {
        let Text(text) = Text(text.parse().map_err(|_| InvalidTextError)?);
        proto::is_valid_text(&text)
            .then(|| Text(text))
            .ok_or(InvalidTextError)
    }
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
pub enum Key {
    Off,
    On,
    Esc,
    Enter,
    Right,
    Left,
    Up,
    Down,
}

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub struct Keys {
    pub last: Option<Key>,
    pub pressed: Integer,
}

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub struct Id {
    pub version: u8,
    pub release: u8,
    pub serial: Integer,
}

impl fmt::Display for Id {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            formatter,
            "PR42{:02}{:02}{:05}",
            self.version, self.release, self.serial
        )
    }
}

//TODO
pub struct Current;

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub struct Setpoint {
    pub value: i32,
    pub decimal_point: u8,
}

//TODO
pub struct External;

bitflags! {
    pub struct Status: u32 {
        const COMERR          = 1 << 0;
        const UNDERRANGE_AIN0 = 1 << 1;
        const OVERRANGE_AIN0  = 1 << 2;
        const UNDERRANGE_AIN1 = 1 << 3;
        const OVERRANGE_AIN1  = 1 << 4;
        const RELAY0          = 1 << 5;
        const RELAY1          = 1 << 6;
        const USER            = 1 << 7;
    }
}

#[derive(Copy, Clone)]
pub enum Tag {
    Setpoint,
    Valve,
    Channel,
    Pressure,
    External,
}

pub struct Line {
    pub tag: Tag,
    pub channel: param::Channel,
}

//TODO
pub struct Range;

//TODO
pub struct Gain;

//TODO
pub struct Offset;

//TODO
pub struct VoltageLevel;

#[derive(Copy, Clone)]
pub enum SignalMode {
    Meter,
    Off,
    Independent,
    External,
    Slave,
    RangeTurndown,
}

//TODO
pub struct Scale;

#[derive(Copy, Clone)]
pub enum LimitMode {
    Sleep,
    Limit,
    Band,
    LimitWithMemory,
    BandWithMemory,
}

//TODO
pub struct DeadBand;

//TODO
pub struct LimitBound;

#[derive(Copy, Clone)]
pub enum Parity {
    None,
    Even,
    Odd,
}

#[derive(Copy, Clone)]
pub enum BaudRate {
    Baud110,
    Baud1200,
    Baud2400,
    Baud4800,
    Baud9600,
    Baud19200,
    Baud38400,
    Baud57600,
    Baud76800,
    Baud110000,
}

#[derive(Copy, Clone)]
pub enum Resolution {
    Bits12,
    Bits16,
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn integer() {
        assert_eq!(Integer::try_from(12345u32), Ok(Integer(12345)));
        assert_eq!(Integer::try_from(99999u32), Ok(Integer(99999)));
        assert_eq!(Integer::try_from(100000u32), Err(IntegerOverflowError));
    }

    #[test]
    fn text() {
        assert_eq!(Text::try_from("\r"), Err(InvalidTextError));
        assert_eq!(Text::try_from("test\0text"), Err(InvalidTextError));
        assert_eq!(
            Text::try_from("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"),
            Err(InvalidTextError),
        );

        assert_eq!(
            Text::try_from("'test.text'"),
            Ok(Text("'test.text'".into()))
        );
    }
}
