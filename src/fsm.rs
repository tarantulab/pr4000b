use core::{
    fmt::{self, Debug, Write},
    marker::PhantomData,
};

use crate::{
    error::DeviceError,
    param::{Channelled, Global, Param, Reset, Set},
    proto::{
        Body, Input, Output, Serial, SerialRead, SerialWrite, MAX_CMD_BODY_LENGTH, MAX_REPLY_LENGTH,
    },
};

pub struct Pr4000b<S> {
    state: State,
    io: S,
}

#[non_exhaustive]
#[derive(Clone, Debug, Eq, PartialEq)]
pub enum Error<ReadError, WriteError> {
    Protocol,
    OutOfSync,
    FullBuffer,
    Device(DeviceError),
    SerialRead(ReadError),
    SerialWrite(WriteError),
}

pub type Result<T, S> =
    nb::Result<T, Error<<S as SerialRead<u8>>::Error, <S as SerialWrite<u8>>::Error>>;

#[derive(Debug)]
enum State {
    Ready,
    Command(heapless::Vec<u8, MAX_CMD_BODY_LENGTH>),
    SendCr,
    Flush,
    Reply(heapless::Vec<u8, MAX_REPLY_LENGTH>),
    CommandTooLong,
}

impl<S> Pr4000b<S> {
    //TODO: Check ?ID on initialization
    pub fn new(io: S) -> Self {
        Pr4000b {
            state: State::Ready,
            io,
        }
    }

    pub fn param<P: Param>(&mut self, param: P) -> AccessGlobal<S, P> {
        AccessGlobal {
            device: self,
            param,
        }
    }
}

impl<S> Debug for Pr4000b<S> {
    fn fmt(&self, fmt: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(fmt, "{:?}", self.state)
    }
}

#[must_use]
pub struct AccessGlobal<'a, S, P> {
    device: &'a mut Pr4000b<S>,
    param: P,
}

impl<'a, S: Serial, P> AccessGlobal<'a, S, P> {
    pub fn channel<C>(self, channel: C) -> AccessChannel<'a, S, P, C>
    where
        P: Channelled<C>,
    {
        AccessChannel {
            device: self.device,
            param: self.param,
            channel,
        }
    }
}

impl<'a, S: Serial, P: Global> AccessGlobal<'a, S, P>
where
    Body<<P as crate::param::Param>::Type>: core::str::FromStr,
{
    pub fn read(self) -> Poll<'a, S, P::Type>
    where
        P::Type: Input,
    {
        self.device
            .exec(|| Command::begin(Escape::for_read::<P>(), self.param.prefix()).build())
    }

    pub fn set(self, value: P::Type) -> Poll<'a, S, P::Echo>
    where
        P: Set,
        P::Type: Output,
        P::Echo: Input,
    {
        self.device.exec(|| {
            Command::begin(Escape::None, self.param.prefix())
                .argument(value)
                .build()
        })
    }

    pub fn reset(self) -> Poll<'a, S, ()>
    where
        P: Reset,
        P::Type: Output,
    {
        self.device
            .exec(|| Command::begin(Escape::Reset, self.param.prefix()).build())
    }
}

#[must_use]
pub struct AccessChannel<'a, S, P, C> {
    device: &'a mut Pr4000b<S>,
    param: P,
    channel: C,
}

impl<'a, S: Serial, P: Channelled<C>, C: Output> AccessChannel<'a, S, P, C> {
    pub fn read(self) -> Poll<'a, S, P::Type>
    where
        P::Type: Input,
    {
        self.device.exec(|| {
            Command::begin_channelled(Escape::for_read::<P>(), self.param.prefix(), self.channel)
                .build()
        })
    }

    pub fn set(self, value: P::Type) -> Poll<'a, S, P::Echo>
    where
        P: Set,
        P::Type: Output,
        P::Echo: Input,
    {
        self.device.exec(|| {
            Command::begin_channelled(Escape::None, self.param.prefix(), self.channel)
                .argument(value)
                .build()
        })
    }

    pub fn reset(self) -> Poll<'a, S, ()>
    where
        P: Reset,
        P::Type: Output,
    {
        self.device.exec(|| {
            Command::begin_channelled(Escape::Reset, self.param.prefix(), self.channel).build()
        })
    }
}

#[must_use]
pub struct Poll<'a, S, R> {
    device: &'a mut Pr4000b<S>,
    in_sync: bool,
    _phantom: PhantomData<R>,
}

impl<S: Serial, R> Poll<'_, S, R> {
    pub fn poll(&mut self) -> Result<R, S>
    where
        R: Input,
    {
        let reply = loop {
            match &mut self.device.state {
                State::Command(queue) => {
                    if let Some(byte) = queue.last() {
                        write(&mut self.device.io, *byte)?;
                        queue.pop();
                    } else {
                        self.device.state = State::SendCr;
                    }
                }

                State::SendCr => {
                    write(&mut self.device.io, b'\r')?;
                    self.device.state = State::Flush;
                }

                State::Flush => {
                    flush(&mut self.device.io)?;
                    self.device.state = State::Reply(Default::default());
                }

                State::Reply(reply) => match read(&mut self.device.io)? {
                    b'\r' => break core::mem::take(reply),

                    byte => {
                        reply
                            .push(byte)
                            .map_err(|_| nb::Error::from(Error::FullBuffer))?;
                    }
                },

                State::CommandTooLong => {
                    self.device.state = State::Ready;
                    return Err(Error::FullBuffer.into());
                }

                _ => return Err(Error::OutOfSync.into()),
            }
        };

        let result = if !self.in_sync {
            Err(Error::OutOfSync)
        } else {
            match core::str::from_utf8(&reply) {
                Ok(reply) => match reply.parse() {
                    Ok(error) => Err(Error::Device(error)),

                    Err(()) => match reply.parse() {
                        Ok(Body(value)) => Ok(value),
                        Err(()) => Err(Error::Protocol),
                    },
                },

                Err(_) => Err(Error::Protocol),
            }
        };

        self.device.state = State::Ready;
        result.map_err(Into::into)
    }
}

impl<S, R> Debug for Poll<'_, S, R> {
    fn fmt(&self, fmt: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(fmt, "{:?}", self.device)
    }
}

impl<S: Serial> Pr4000b<S> {
    fn exec<R: Input, F: FnOnce() -> State>(&mut self, on_ready: F) -> Poll<S, R> {
        let in_sync = match self.state {
            State::Ready => {
                self.state = on_ready();
                true
            }

            _ => false,
        };

        Poll {
            device: self,
            in_sync,
            _phantom: PhantomData,
        }
    }
}

type CommandBuffer = heapless::String<MAX_CMD_BODY_LENGTH>;
struct Command(Option<CommandBuffer>);

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
enum Escape {
    None,
    Query,
    Reset,
}

impl Escape {
    pub fn for_read<P: Param>() -> Self {
        if P::ESCAPED_READ {
            Escape::Query
        } else {
            Escape::None
        }
    }
}

impl Command {
    pub fn begin(escape: Escape, prefix: &str) -> Self {
        Command(Some(Default::default())).map(|buffer| {
            match escape {
                Escape::None => (),
                Escape::Query => write!(buffer, "?")?,
                Escape::Reset => write!(buffer, "!")?,
            }

            write!(buffer, "{}", prefix)
        })
    }

    pub fn begin_channelled(escape: Escape, prefix: &str, channel: impl Output) -> Self {
        Self::begin(escape, prefix).map(|buffer| write!(buffer, "{}", Body(channel)))
    }

    pub fn argument<T: Output>(self, value: T) -> Self {
        self.map(|buffer| write!(buffer, ",{}", Body(value)))
    }

    pub fn build(self) -> State {
        match self.0 {
            Some(command) => {
                let mut command = command.into_bytes();
                command.reverse();

                State::Command(command)
            }

            None => State::CommandTooLong,
        }
    }

    fn map<F>(self, writer: F) -> Self
    where
        F: FnOnce(&mut CommandBuffer) -> fmt::Result,
    {
        Command(
            self.0
                .and_then(|mut buffer| writer(&mut buffer).ok().map(|()| buffer)),
        )
    }
}

fn read<S: Serial>(serial: &mut S) -> Result<u8, S> {
    serial.read().map_err(|error| error.map(Error::SerialRead))
}

fn write<S: Serial>(serial: &mut S, byte: u8) -> Result<(), S> {
    serial
        .write(byte)
        .map_err(|error| error.map(Error::SerialWrite))
}

fn flush<S: Serial>(serial: &mut S) -> Result<(), S> {
    serial
        .flush()
        .map_err(|error| error.map(Error::SerialWrite))
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{param::*, proto::Ops};

    #[test]
    fn escape() {
        assert_eq!(Escape::for_read::<Id>(), Escape::Query);
        assert_eq!(Escape::for_read::<Gain>(), Escape::Query);
        assert_eq!(Escape::for_read::<Valve>(), Escape::Query);
        assert_eq!(Escape::for_read::<Status>(), Escape::None);
        assert_eq!(Escape::for_read::<Actual>(), Escape::None);
        assert_eq!(Escape::for_read::<External>(), Escape::None);
    }

    #[test]
    fn commands() {
        assert_eq!(
            Command::begin(Escape::Query, Ops::prefix(&Id)).0,
            Some("?ID".into())
        );

        assert_eq!(
            Command::begin(Escape::Reset, Ops::prefix(&(Display2, Line1))).0,
            Some("!DP2,1".into())
        );

        assert_eq!(
            Command::begin(Escape::None, Ops::prefix(&Display1))
                .argument(false)
                .0,
            Some("DP1,OFF".into())
        );

        assert_eq!(
            Command::begin(Escape::None, Ops::prefix(&Status)).0,
            Some("ST".into())
        );

        assert_eq!(
            Command::begin_channelled(Escape::None, Ops::prefix(&Valve), Channel2)
                .argument(true)
                .0,
            Some("VL2,ON".into())
        );
    }
}
