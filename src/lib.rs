#![no_std]

pub use pr4000b_proc::*;

pub mod data;
pub mod param;

pub use {
    error::{Error, Result},
    fsm::Pr4000b,
    proto::{Serial, SerialRead, SerialWrite},
};

pub mod cmd {
    pub use crate::fsm::{AccessChannel, AccessGlobal, Poll};
}

pub mod error {
    pub use crate::{
        fsm::{Error, Result},
        proto::{DeviceError, DeviceErrorClass, ReadError, WriteError},
    };
}

mod fsm;
mod proto;

mod sealed {
    pub trait Sealed {}
    impl<T: ?Sized> Sealed for T {}
}

use sealed::Sealed;
